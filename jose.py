import logging
import random
import time
import asyncio
import pathlib
import importlib
import collections

import discord
import aiohttp

import uvloop

from discord.ext import commands
from discord.ext.commands.errors import ExtensionAlreadyLoaded

import joseconfig as config
from ext.common import SayException
from ext.utils.help import PMHelpCommand

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

log = logging.getLogger(__name__)

extensions = [
    "channel_logging",  # loading at start to get the logger to run
    "config",
    "admin",
    "eval",
    "state",
]


class JoseContext(commands.Context):
    @property
    def member(self):
        """Get the member of a message. If any."""
        # This is inneficient, isn't it?
        if self.guild is None:
            return None

        return self.guild.get_member(self.author.id)

    async def ok(self):
        """Send an OK signal to the user."""
        try:
            await self.message.add_reaction("👌")
        except discord.Forbidden:
            await self.message.channel.send("👌")

    async def not_ok(self):
        """Send a NOT OK signal to the user."""
        try:
            await self.message.add_reaction("❌")
        except discord.Forbidden:
            await self.message.channel.send("❌")

    async def success(self, flag):
        """Call ok() or not() depending on flag."""
        if flag:
            await self.ok()
        else:
            await self.not_ok()

    async def status(self, flag):
        """Alias to success()"""
        await self.success(flag)

    async def err(self, msg):
        """Send an error-themed message."""
        await self.send(f"\N{POLICE CARS REVOLVING LIGHT} {msg}")

    @property
    def clean_content(self):
        """Get a clean content version of the message"""
        return self.bot.clean_content(self.message.content)

    async def send(self, content=None, **kwargs):
        # FUCK EACH AND @EVERYONE OF YOU
        # specially mary and gerd

        # i hope this saves my life, forever.
        content = content or ""
        new_cnt = self.bot.clean_content(content, normal_send=True)
        return await super().send(new_cnt, **kwargs)

    def send_bg(self, content: str):
        """Send a message in the background."""
        future = self.send(content)
        self.bot.loop.create_task(future)


class JoseBot(commands.Bot):
    """Main bot subclass."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.init_time = time.time()
        self.config = config
        self.session = aiohttp.ClientSession()

        #: Exceptions that will be simplified
        #   to WARN logging instead of ERROR logging
        self.simple_exc = [SayException]

        #: used by ext.channel_logging
        self.channel_handlers = []

        #: blocking stuff
        self.block_cache = {}

    async def on_ready(self):
        """Bot ready handler"""
        log.info(f"Logged in! {self.user!s}")

    async def is_blocked(self, user_id: int, key: str = "user_id") -> bool:
        """Returns if something blocked to use José. Uses cache"""
        if user_id in self.block_cache:
            return self.block_cache[user_id]

        blocked = await self.db.fetchval(
            f"""
        SELECT reason
        FROM blocks
        WHERE {key} = $1
        """,
            user_id,
        )

        is_blocked = bool(blocked)
        self.block_cache[user_id] = is_blocked

        return is_blocked

    async def is_blocked_guild(self, guild_id: int) -> bool:
        """Returns if a guild is blocked to use José. Uses cache"""
        return await self.is_blocked(guild_id, "guild_id")

    def clean_content(self, content: str, **kwargs) -> str:
        """Make a string clean of mentions and not breaking codeblocks"""
        content = str(content)

        # only escape codeblocks when we are not normal_send
        # only escape single person pings when we are not normal_send
        if not kwargs.get("normal_send", False):
            content = content.replace("`", r"\`")
            content = content.replace("<@", "<@\u200b")
            content = content.replace("<#", "<#\u200b")

        # always escape role pings (@everyone) and @here
        content = content.replace("<@&", "<@&\u200b")
        content = content.replace("@here", "@\u200bhere")
        content = content.replace("@everyone", "@\u200beveryone")

        return content

    async def on_message(self, message):
        if message.author.bot:
            return

        author_id = message.author.id
        if await self.is_blocked(author_id):
            return

        if message.guild is not None:
            guild_id = message.guild.id

            if await self.is_blocked_guild(guild_id):
                return

        ctx = await self.get_context(message, cls=JoseContext)
        await self.invoke(ctx)

    def load_extension(self, name: str):
        """wrapper for Bot.load_extension"""
        log.debug(f"[load:loading] {name}")
        t_start = time.monotonic()

        try:
            super().load_extension(name)
        except ExtensionAlreadyLoaded:
            log.warning(f"ext {name} already loaded")
        t_end = time.monotonic()

        delta = round((t_end - t_start) * 1000, 2)
        log.info(f"[load] {name} took {delta}ms")

    def add_jose_cog(self, cls: "class"):
        """Add a cog but load its requirements first."""
        requires = cls.cog_metadata.get("requires", [])

        log.debug("requirements for %s: %r", cls, requires)
        if not requires:
            log.debug(f"no requirements for {cls}")

        for _req in requires:
            req = f"ext.{_req}"
            if req in self.extensions:
                log.debug("loading %r from requirements", req)
                self.load_extension(req)
            else:
                log.debug("%s is already loaded", req)

        # We instantiate here because
        # instantiating on the old add_cog
        # is exactly the cause of the problem
        cog = cls(self)
        super().add_cog(cog)

    def load_all(self):
        """Load all extensions in the extensions folder.

        Thanks FrostLuma for code!
        """

        for extension in extensions:
            self.load_extension(f"ext.{extension}")

        path = pathlib.Path("ext/")
        files = path.glob("**/*.py")

        for fileobj in files:
            if fileobj.stem == "__init__":
                name = str(fileobj)[:-12]
            else:
                name = str(fileobj)[:-3]

            name = name.replace("/", ".")
            module = importlib.import_module(name)

            if not hasattr(module, "setup"):
                # ignore extensions that do not have a setup() function
                continue

            if name in extensions:
                log.debug(f"ignoring {name}")

            try:
                self.load_extension(name)
            except ExtensionAlreadyLoaded:
                log.warning(f"ext {extension} already loaded, skipping")

    @property
    def owner(self):
        """Return the user object representing the owner."""
        return self.get_user(self.owner_id)


async def get_prefix(bot, message) -> list:
    """Get the preferred list of prefixes for a determined guild/dm."""
    if not message.guild:
        return bot.config.prefix

    config_cog = bot.get_cog("Config")
    if not config_cog:
        log.warning("config cog not found")
        return [config.prefix]

    custom = await config_cog.cfg_get(message.guild, "prefix")
    if custom == bot.config.prefix:
        return custom

    # sort backwards due to the command parser taking the first match
    return sorted([bot.config.prefix, custom], reverse=True)


def main():
    """Main entry point"""
    jose = JoseBot(
        command_prefix=get_prefix,
        description="josé is a multipurpose bot full of stuff",
        owner_id=getattr(config, "owner_id", None),
        help_command=PMHelpCommand(),
    )

    jose.load_all()
    jose.run(config.token)


if __name__ == "__main__":
    main()
