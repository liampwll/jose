import io
import json
import logging
import zipfile

import discord
from discord.ext import commands

from .common import Cog, SayException
from .utils import Timer

log = logging.getLogger(__name__)


async def write_json(zip_file, fpath: str, obj):
    objstr = json.dumps(obj, indent=4)
    zip_file.writestr(fpath, objstr)


class StarboardArchiver(Cog):
    """Archival functions for starboard."""

    def __init__(self, bot):
        super().__init__(bot)

        self._jobs = {}

    async def _write_allowed_chans(self, zip_file, cfg):
        rows = await self.pool.fetch(
            """
        SELECT channel_id
        FROM starconfig_allow
        WHERE guild_id = $1
        """,
            cfg["guild_id"],
        )

        rows = [r[0] for r in rows]

        await write_json(zip_file, "allowed_channels.json", rows)

    async def _write_main_sb_data(self, zip_file, cfg):
        stars = self.bot.get_cog("Starboard")

        rows = await self.pool.fetch(
            """
        SELECT message_id, channel_id, author_id, star_message_id
        FROM starboard
        WHERE guild_id = $1
        ORDER BY star_message_id ASC
        """,
            cfg["guild_id"],
        )

        res = {}

        for row in rows:
            message_id = row["message_id"]

            res[message_id] = {
                "channel_id": row["channel_id"],
                "author_id": row["author_id"],
                "star_message_id": row["star_message_id"],
                "starrers": await stars.get_starrers(message_id),
            }

        await write_json(zip_file, "main.json", res)

    async def _archiver(self, cfg: dict, ctx):
        """Main starboard archiver worker."""
        temporary_fd = io.BytesIO()

        zip_file = zipfile.ZipFile(
            temporary_fd, mode="w", compression=zipfile.ZIP_DEFLATED
        )

        await write_json(zip_file, "config.json", cfg)

        with Timer() as t_sb_allow:
            await self._write_allowed_chans(zip_file, cfg)

        with Timer() as t_sb_main:
            await self._write_main_sb_data(zip_file, cfg)

        zip_file.close()
        temporary_fd.seek(0)

        # send file to discord
        attachment = discord.File(temporary_fd, filename=f"sb-dump-{ctx.guild.id}.zip")

        await ctx.send(
            "finished!\n"
            "timers:\n"
            f"\tallowed_channels.json: {t_sb_allow}\n"
            f"\tmain storage: {t_sb_main}\n",
            file=attachment,
        )

    async def _archiver_wrapper(self, cfg, ctx):
        try:
            log.info("starting archiver for guild %d", ctx.guild.id)
            await self._archiver(cfg, ctx)
        except Exception as err:
            log.exception("error while archiving")
            await ctx.send(f"error while archiving: {err!r}")
        finally:
            self._jobs.pop(ctx.guild.id)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def sbarchive(self, ctx):
        """Start an archival job for the guild.

        This can take a long time.
        """
        if ctx.guild.id in self._jobs:
            raise SayException("There is already an archival job for the guild")

        stars = self.bot.get_cog("Starboard")

        cfg = await stars.get_starconfig(ctx.guild.id)

        if cfg is None:
            raise SayException("No starboard configuration found")

        self._jobs[ctx.guild.id] = self.loop.create_task(
            self._archiver_wrapper(cfg, ctx)
        )

        await ctx.ok()


def setup(bot):
    bot.add_jose_cog(StarboardArchiver)
